/*
 * sysTick.c
 *
 *  Created on: Sep 24, 2012
 *      Author: renan
 */

#include "sysTick.h"


volatile uint32_t msTicks; // counter for 1ms SysTicks

// ****************
//  SysTick_Handler - just increment SysTick counter
void SysTick_Handler (void)
{
	msTicks++;
}

/**
 * @brief This routine is used to create a delay.
 * Creates a delay of the appropriate number of Systicks (happens every 1 ms)
 *
 * @param delayTicks Number of "miliseconds" to delay.
 */
void msDelay (uint32_t delayTicks)
{
	uint32_t currentTicks;

	currentTicks = msTicks;	// read current tick counter
	// Now loop until required number of ticks passes.
	while ((msTicks - currentTicks) < delayTicks);
}

void usDelay (uint32_t time)
{
    int a;
    for(a = 0; a < time; a++)
    	waitMicroSeg;
}

/**
 * @brief This routine is used to create initialize the SysTick configuration.
 *
 * @return     none
 */
void initSysTick ()
{
	// Setup SysTick Timer to interrupt at 1 msec intervals
	if (SysTick_Config(SystemCoreClock / 1000))
	{
		while (1);  // Capture error
	}
}
