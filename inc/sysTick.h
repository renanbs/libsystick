/*
 * sysTick.h
 *
 *  Created on: Sep 24, 2012
 *      Author: renan
 */

#ifndef SYSTICK_H_
#define SYSTICK_H_

#include "LPC17xx.h"
#include "types.h"

/**
*  @brief Micro seconds delay
*  @param none
*/
#define waitMicroSeg __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();

void SysTick_Handler (void);
void msDelay (uint32_t delayTicks);
void usDelay (uint32_t time);
void initSysTick ();

#endif // SYSTICK_H_
